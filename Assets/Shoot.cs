﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{

    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private Rigidbody bulletPrefab;
    [SerializeField]
    private float launchForce = 700f;

    public void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            Launchrocket();
        }
    }
    private void Launchrocket()
    {
        var rocketInstance = Instantiate(
            bulletPrefab,
            firePoint.position,
            firePoint.rotation);

        rocketInstance.AddForce(firePoint.up * launchForce);
    }
}
