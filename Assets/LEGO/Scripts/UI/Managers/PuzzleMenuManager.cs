using Unity.LEGO.Game;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Unity.LEGO.UI
{
    // This is the component that handles the the InGameMenu.
    // Press TAB at runtime to show the menu.

    public class PuzzleMenuManager : MonoBehaviour
    {
        [Header("References")]

        [SerializeField, Tooltip("The canvas that holds the menu.")]
        GameObject m_Menu = default;

        [SerializeField, Tooltip("KeyCode Press .")]
        public KeyCode m_kCode; 
        
        void Start()
        {      
            m_Menu.SetActive(false);  
        }

        public void ClosePauseMenu()
        {
            SetPauseMenuActivation(false);
        }
        
        public void TogglePauseMenu()
        {
            SetPauseMenuActivation(!(m_Menu.activeSelf));
        }


        void Update()
        {

            // Custom Key
            if (Input.GetKeyDown(m_kCode))
            {
                TogglePauseMenu();
            }

        }

        void SetPauseMenuActivation(bool active)
        {
#if !UNITY_EDITOR
            Cursor.lockState = active ? CursorLockMode.None : CursorLockMode.Locked;
#endif

            m_Menu.SetActive(active);

            if (m_Menu.activeSelf)
            {
                Time.timeScale = 0f;

                EventSystem.current.SetSelectedGameObject(null);
            }
            else
            {
                Time.timeScale = 1f;
            }

            OptionsMenuEvent evt = Events.OptionsMenuEvent;
            evt.Active = active;
            EventManager.Broadcast(evt);
        }
    }
}