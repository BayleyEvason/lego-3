﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public static int Lives;
    public int startLives = 20;

    public static int Points;
    public int startPoints = 0;

    public static int Rounds;

    private void Start()
    {
        Lives = startLives;
        Points = startPoints;

        Rounds = 0;
    }
}
