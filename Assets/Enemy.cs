﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public ParticleSystem explosion;

    public float speed = 10f;

    public int health = 100;

    public int value = 50;

    private Transform target;
    private int WavePointIndex = 0;

    public Image healthBar;
    private void Start()
    {
        target = WavePoint.points[0];
    }

    public void TakeDamage(int amount)
    {
        health -= amount;

        healthBar.fillAmount = health / 100f;

        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        PlayerStats.Points += value;

        WaveSpawner.EnemiesAlive--;

        Instantiate(explosion,transform.position,Quaternion.identity).Play();
        Destroy(gameObject);
    }

    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }
    }
     void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Rocket") 

        {
            TakeDamage(10);
            //health -= 10;
            
        }
        if (collider.gameObject.tag == "Bullet")
        {
            TakeDamage(5);
            //health -=5;
        }
    }

    void GetNextWaypoint()
    {
        if (WavePointIndex >= WavePoint.points.Length - 1)
        {
            EndPath();
            return;
        }

        WavePointIndex++;
        target = WavePoint.points[WavePointIndex];
    }

    void EndPath ()
    {
        PlayerStats.Lives--;
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
    }

}
