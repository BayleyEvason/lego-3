﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private Rigidbody rocketPrefab;
    [SerializeField]
    private float launchForce = 700f;

    public void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Launchrocket();
        }
    }
    private void Launchrocket()
    {
        var rocketInstance = Instantiate(
            rocketPrefab,
            firePoint.position,
            firePoint.rotation);

        rocketInstance.AddForce(firePoint.up * launchForce);
        
    }
}
