﻿using UnityEngine;

public class Rocket : MonoBehaviour
{
    private Transform target;


    public float lifetime;

    private void Start()
    {
        Destroy(gameObject, lifetime);
    }

    public void Seek(Transform _target)

    {
        target = _target;

    }

    // Update is called once per frame
    
}
