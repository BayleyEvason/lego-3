﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldBorder : MonoBehaviour
{
    
    public static bool GameIsOver;

    public GameObject gameOverUI;
    void Start()
    {
        GameIsOver = false;
        
    }

    private void OnTriggerEnter()
    {
        GameIsOver = true;
        gameOverUI.SetActive(true);
    }
}