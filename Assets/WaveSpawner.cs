﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WaveSpawner : MonoBehaviour
{
    public GameObject WonGame;
    public static int EnemiesAlive = 0;

    public Transform EnemyPrefab;

    public Transform spawnPoint;

    public float timeBetweenWaves = 30f;
    private float countdown = 5f;

    public Text waveCountdownText;

    private int waveIndex = 0;
    void Update()
    {
        if (EnemiesAlive > 0)
        {
            return;
        }

        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;

        }

        countdown -= Time.deltaTime;

        waveCountdownText.text = Mathf.Round(countdown).ToString();

    }
    IEnumerator SpawnWave ()
    {
        waveIndex++;
        PlayerStats.Rounds++;

        for (int i= 0; i < waveIndex; i++)
        {
            SpawnEnemy3();
            yield return new WaitForSeconds(5f);
        }

        if (waveIndex == 26)
        {
            WonGame.SetActive(true);
            Debug.Log("LEVEL WON!");
        }
    }
    void SpawnEnemy3()
    {
        Instantiate(EnemyPrefab, spawnPoint.position, spawnPoint.rotation);
        EnemiesAlive++;
    }
}
